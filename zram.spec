Name:		zram
Summary:	ZRAM for swap config and services for CentOS 7
Summary(ru_RU.UTF-8): Служба, настраивающая использование ZRAM подкачки
Version:	0.2
Release:	1%{?dist}
License:	GPLv2+
URL:		https://gitlab.com/pda_rpms/zram-centos7
Group:		System Environment/Daemons
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	COPYING
Source1:	zram.conf
Source2:	zram-swap.service
Source3:	zramstart
Source4:	zramstop
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
%{?systemd_requires}
BuildRequires: systemd
Requires: gawk grep glibc-common util-linux >= 2.23.2-52

%description
ZRAM is a Linux block device that can be used for compressed swap in memory.
It's useful in memory constrained devices. This provides a service to setup
ZRAM as a swap device based on criteria such as available memory.

%description -l ru_RU.UTF-8
ZRAM это блочное устройство Linux, используемое для создания сжатой области
подкачки в памяти. Оно подходит для устройств, имеющих ограниченный объём
памяти. Эта служба предоставляет возможность настроить использование ZRAM
по таким критериям, как доступная память.

%prep
%{__cp} %{SOURCE0} %{_builddir}/COPYING

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/

%{__mkdir_p} %{buildroot}%{_unitdir}/
%{__install} -pm 0644 %{SOURCE2} %{buildroot}%{_unitdir}/

%{__mkdir_p} %{buildroot}%{_sbindir}
%{__install} -pm 0755 %{SOURCE3} %{buildroot}%{_sbindir}
%{__install} -pm 0755 %{SOURCE4} %{buildroot}%{_sbindir}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__rm} %{_builddir}/COPYING

%post
%systemd_post zram-swap.service

%postun
%systemd_postun zram-swap.service

%files
%license COPYING
%config(noreplace) %{_sysconfdir}/%{name}.conf
%{_unitdir}/zram-swap.service
%{_sbindir}/zramstart
%{_sbindir}/zramstop

%changelog
* Thu Nov 22 2018 Dmitriy Pomerantsev <pda2@yandex.ru>
- Backported to CentOS 7.

* Thu Jul 19 2018 Peter Robinson <pbrobinson@fedoraproject.org> 0.2-1
- Service ordering fixes, minor cleanup

* Tue Jul 17 2018 Peter Robinson <pbrobinson@fedoraproject.org> 0.1-1
- Initial package
